% !TeX spellcheck = en_US
\chapter{A Characterization for Regular Text Series}
\label{ch:nivat}
\allowdisplaybreaks
In the last two chapters, we gained an overview of weighted and unweighted branching and parenthesizing automata. In the last part of this thesis, we would like to strengthen the connection between weighted and unweighted automata. Such an approach was originally taken by Nivat \cite{nivat68}, who characterized word-to-word transducers. He showed that the behavior of these machines, which can be seen as weighted word automata over the semiring of formal languages, can be exactly described by a regular word language over a new alphabet and a single-state transducer together with a homomorphism. 
This approach is now a well known result, which was transferred to many other automaton models. In the first section, we will take a brief look at the results for weighted word automata \cite{droste_kuske21} and weighted tree automata \cite{götze17}. 
A certainly not conclusive list of models for which a similar result was proven contains picture automata \cite{babari15}, timed automata \cite{perevoshchikov14}, $\omega$-pushdown automata \cite{droste_dziadek_kuich20} and even a very general result for graph automata \cite{dück_droste15}.
In \cref{sec:nivat_wbpa_problems} we discuss the problems occurring while developing a way of adapting Nivat's theorem to weighted branching and parenthesizing automata. In an attempt to overcome these difficulties we make use of an idea from \cite{grabolle21} and relations between texts and trees from Mathissen \cite{mathissen09}. This gives us a way to prove a Nivat-like theorem for weighted branching and parenthesizing automata in \cref{sec:nivat_wbpa_wta}, connecting them to weighted tree automata, which are already well researched \cite{fülop_vogler09}.
\section{Nivat-like Theorems for Weighted Word and Tree Automata}
\label{sec:nivat_words_trees}
Nivat's original theorem \cite{nivat68} characterizes word-to-word transducers. In \cite{droste_kuske21} Droste and Kuske stated a more general Nivat theorem for weighted word automata over arbitrary semirings. For the convenience of the reader, we give the statement here. For details and definitions see \cite{droste_kuske21}.
\begin{thm}[Nivat-like theorem for weighted word automata]
	A word series $s:\Sigma^*\to K$ is recognized by a weighted automaton if and only if there exists an alphabet $\Lambda$, a non-deleting homomorphism $h:\Lambda^*\to\Sigma^*$, a regular language $L\subseteq\Lambda^*$, and a weighted automaton $\A_w$ over $\Lambda$ with exactly one state such that:
	\begin{equation*}
		s=h(\behav{\A_w}\hadamard\charac_L).
	\end{equation*}
\end{thm}

As texts are closely related to trees \cite[Section 4]{ehrenfeucht_rozenberg93} we are interested in a Nivat-like theorem for weighted tree automata. This result will be of much use in the next section. 
A direct generalization of the above theorem to weighted unranked tree automata was proven in \cite[Theorem 12]{götze17}. We restate it here for the special case of weighted ranked tree automata and shortly give the needed definitions from \cite{tata07,fülop_vogler09}.
\begin{defn}
	A \textit{ranked alphabet} is an alphabet $\Sigma$ equipped with a mapping $\rk:\Sigma\to\N$.
	A \textit{(ranked) tree} $t$ is a mapping from a finite, non-empty, prefix-closed set $\pos(t)\subseteq\N^*$ into a ranked alphabet, with respect to $\rk$. 
	This means that for any $p\in\pos(t)$ with $\rk(t(p))=n$ we have $\{j\mid pj\in\pos(t)\}=\{1,\dots,n\}$ ($\emptyset$ for $n=0$).
	A tree could also be represented by a \textit{term} $g(t_1,\dots,t_k)$, where $\pos(t)$ is defined according to the term structure, $t(\varepsilon)=g$ and $t(p),p\in\pos(t)\setminus\{\varepsilon\}$ is defined recursively by the terms $t_1,\dots,t_k$.
	The \textit{set of trees over $\Sigma$} is denoted $\trees_{\Sigma}$.
	A tree could contain variables and the \textit{set of trees over $\Sigma$ and variables} $\X_n:=\{x_1,\dots,x_n\}$ is denoted $\trees_\Sigma[\X_n]$.
	
	We can \textit{substitute} a variable $x_i$ in a tree $t=g(t_1,\dots,t_k)\in\trees_\Sigma[\X_n]$ by a tree $t'\in\trees_{\Lambda}[\X_n]$. We denote this by $t\sub[x_i\leftarrow t']:=g(t_1\sub[x_i\leftarrow t'],\dots,t_k\sub[x_i\leftarrow t'])$ and $\sigma\sub[x\leftarrow t']:=\sigma$ and $x\sub[x\leftarrow t']:=t'$.
	We extend the substitution to multiple variables by defining $t\sub*[x_1\leftarrow t_1'][x_n\leftarrow t_n']:=\allowbreak t\sub[x_1\leftarrow t_1']\dots\sub[x_n\leftarrow t_n']$.
	
	A mapping $h:\trees_{\Sigma}\to\trees_\Lambda$ is called a \textit{tree homomorphism} if for all $g\in\Sigma$ with rank $n$ there exists a $t_g\in\trees_{\Lambda}[\X_n]$ with $h(g(t_1,\dots,t_n))=t_g\sub*[x_1\leftarrow h(t_1)][x_n\leftarrow h(t_n)]$ for all $t_1,\dots,t_n\in\trees_{\Sigma}$. 
	We will denote $t_g$ by $h(g)$, even though $t_g$ is not necessarily in $\trees_\Lambda$.
	A tree homomorphism $h$ is called \textit{non-deleting} (resp. \textit{linear}), if for all $g\in\Sigma$ with rank $n$, the tree $h(g)$ contains at least one symbol from $\Lambda$ and all variables of $\X_n$ occur at least (resp. exactly) once in $h(g)$.
	
	\textit{(Weighted) ranked tree automata} and \textit{regular tree languages (resp. tree series)} are defined as in \cite{fülop_vogler09}.
	\textit{Hadamard product} and \textit{characteristic series} are defined analogously to the case of texts.
	The \textit{application of a non-deleting tree homomorphism} $h:\trees_{\Sigma}\to\trees_\Lambda$ on a tree series $s:\trees_\Lambda\to K$ is defined by $h(s)(t):=\sum_{t'\in h^{-1}(t)}s(t')$ for all $t\in\trees_\Sigma$. That is $h(s)$ is a tree series from $\trees_\Sigma$ into $K$, where $h(s)(t)$ is calculated by summing the weights of the trees $t'$ which are mapped to $t$ by $h$. The sum is well-defined since $h$ is non-deleting and therefore $h^{-1}(t)$ finite for every $t\in\trees_\Sigma$.
\end{defn}
\begin{thm}[Nivat-like theorem for weighted ranked tree automata]\label{thm:nivat_for_trees}
	A tree series $s:\trees_\Sigma:\to K$ is recognized by a weighted tree automaton if and only if there exist a ranked alphabet $\Lambda$, a linear tree homomorphism $h:\trees_\Lambda\to\trees_\Sigma$, a regular tree language $L\subseteq\trees_\Lambda$, and a weighted tree automaton $\A_w$ over $\Lambda$ with exactly one state such that:
	\begin{equation*}
		s=h(\behav{\A_w}\hadamard\charac_L).
	\end{equation*}
\end{thm}
\begin{proof}
	A proof can be found in \cite[Theorem 5.8]{götze17}.
\end{proof}
\section{Nivat-like Approach for Weighted Branching and Parenthesizing Automata}
\label{sec:nivat_wbpa_problems}
It is obvious to try to adopt the standard proof from \cite{droste_kuske21} or \cites{götze17} to our case. We analyze the problems which arise for WBPAs separately for both proof directions.

For a given weighted (tree) automaton $\A$, the standard proof encodes every run of $\A$ as a word (resp. tree) over a new alphabet. It is shown that this language $L$ containing all encoded runs of $\A$ is regular. The characteristic series of $L$ is therefore regular, too. The associated weights are calculated by the Hadamard product with a single-state weighted (tree) automaton, storing the weights for each transition. Application of the homomorphism $h$ then extracts the actual word (resp. tree) from the transitions. Thus the construction will yield exactly the word (resp. tree) series recognized by $\A$.

Regarding our automata, we would first have to construct a BPA $A_\text{runs}$ which recognizes all successful runs of a given WBPA $\A$. For the sequential case, this is essentially the same construction as in the word case: A transition $(p,(p,a,q),q)$ for every transition $(p,a,q)$ of $\A$. But since a run might also consist of parenthesizing relations, we also need to represent these through sequential transitions to be able to extract the correct parentheses later on. For a closing parenthesizing relation $(p,)_s,q)$ of $\A$ this requires $(p,(p,)_s,q),(p,)_s,q))$ as a sequential transition and $((p,)_s,q),)_s,q)$ as a parenthesizing transition of $A_\text{runs}$.
This can be depicted by:
\begin{center}
	\begin{tikzpicture}[baseline=(1.base), every node/.style={anchor=west}]
		\node[text depth=0pt] (0s) at (-1.25,1.5) {\textbf{type}};
		\node[] (0s) at (0,1.5) {\textbf{transition}};
		\node[] (1s) at (-1,0.5) {$\V$};
		\node[] (2s) at (-1,-0.5) {$\H$};
		\node[] (3s) at (-1,-1.5) {$\H$};
		\node[] (4s) at (-1,-2.5) {$\H$};
		\node[] (5s) at (-1,-3.5) {$\H$};
		\node[] (6s) at (-1,-4.5) {$\V$};
		\node[] (1) at (0,0) {opening parenthesizing transition};
		\node[] (2) at (0,-1) {sequential transition ``writing'' previous parenthesizing transition};
		\node[] (3) at (0,-2) {sequential transition(s)};
		\node[] (4) at (0,-3) {sequential transition ``writing'' following parenthesizing transition};
		\node[] (5) at (0,-4) {closing parenthesizing transition};
		\draw[>=latex,->] (1s) to[bend left=45] (2s);
		\draw[>=latex,->] (2s) to[bend left=45] (3s);
		\draw[>=latex,->] (3s) to[bend left=45] (4s);
		\draw[>=latex,->] (4s) to[bend left=45] (5s);
		\draw[>=latex,->] (5s) to[bend left=45] (6s);
	\end{tikzpicture}.
\end{center}
As we need to ensure that we only encode well-defined runs on $\A$, we have to prohibit the use of double parentheses like $((\ldots))$. This is not automatically ensured by the definition of possible runs of $A_\text{runs}$, because we always have the intermediate sequential transition for ``writing'' the parenthesizing transition. It cannot easily be ensured that these transition pairs do not repeat on both sides of $(\ldots)$ simultaneously by the usual construction for this proof.

Even if a possible construction proving the forward direction exists, we still have to consider the backward direction.  The standard proof utilizes the following properties of word (resp. tree) series:
\begin{enumerate}
	\item $\charac_L$ is regular if $L$ is regular,
	\item closure under Hadamard product,
	\item closure under non-deleting word (resp. linear tree) homomorphisms.
\end{enumerate}
We have already shown the first two properties in \cref{ch:weighted} for text series. Regarding the third one, Mathissen showed at least the closure under inverses of homomorphisms in \cite[Lemma 2.18]{mathissen09}. We could derive a positive result about the closure under suitable text homomorphisms from \cite[Proposition 4.28]{mathissen09} by transferring them to tree homomorphisms. But even with such a result, we would face another problem, special to our WBPAs: The parenthesizing transitions of WBPAs behave like $\varepsilon$-transitions, in the sense that they do not prolong the label of a run (see the third case in \cref{def:weighted_runs}). The homomorphism $h$ described above would have to map a parenthesizing transition (encoded as a text) to the empty text. This small detail impacts heavily because now the expression $h(\behav{\A_w}\hadamard\charac_L)$ is not necessarily well-defined (if we adopt the definition for trees). The involved sum could iterate over an infinite set, leaving unclear what the result should be.

\section{Connection to Weighted Tree Automata}
\label{sec:nivat_wbpa_wta}
%------------------------NIVAT WIE BEI GUSTAV-----------------------
To bypass the above problems we utilize the relation of texts to trees described in \cite[Propositions 4.28, 4.29]{mathissen09} and an idea from \cite{grabolle21}, linking our Nivat-like theorem to the ones for weighted ranked tree automata:
\begin{defn}
	Consider the ranked alphabet $\Sigma\cup\Gamma$ with $\rk(\sigma)=0$ and $\rk(\pi)$ as usual for all $\sigma\in\Sigma,\pi\in\Gamma$.
	For each tree $t=\pi(t_1,\dots,t_k)\in\trees_{\Sigma\cup\Gamma}$ we inductively define a text $\tau_t\in\texts$ by
	\begin{itemize}
		\item $\tau_t=t(\varepsilon)(\tau_{t_1},\dots,\tau_{t_k})$ and
		\item $\tau_{\sigma}=\sigma$ for all $\sigma\in\Sigma$.
	\end{itemize}
	We call $\tau_t$ the \textit{text of $t$} and define the mapping $\ttxt:\trees_{\Sigma\cup\Gamma}\to\texts$ by $\ttxt(t):=\tau_t$.
\end{defn}
	
Note that $\ttxt$ is surjective, but not injective. For a primitive representation of a text $\tau=\pi(\tau_1,\dots,\tau_k)\in\texts$ we can define a tree $t_\tau\in\trees_{\Sigma\cup\Gamma}$ by interpreting the representation of $\tau$ as a term.
Then $\ttxt(t_\tau)=\pi(\tau_1,\dots,\tau_k)=\tau$. But there are possibly multiple preimages of $\tau$ under $\ttxt$ because a primitive representation $\tau=\pi(\tau_1,\dots,\tau_k)$ is not necessarily unique, if cascading $\hor$ or $\ver$ are involved. For an example consider the text $\tau_1\hor\tau_2\hor\tau_3$ which has multiple primitive representations $\tau=\hor(\hor(\tau_1,\tau_2),\tau_3)=\hor(\tau_1,\hor(\tau_2,\tau_3))$, both resulting in a different tree (analogously for $\ver$).

\begin{defn}
	We now fix a specific preimage for each text $\tau$ under $\ttxt$, by choosing the primitive representation where the parentheses are in the right-most form, i.e. the second form from the above example. In other words the chosen representation must  contain neither $\hor(\hor(\tau_1,\tau_2),\tau_3)$ nor $\ver(\ver(\tau_1,\tau_2),\tau_3)$. We call this preimage the \textit{r-shape of $\tau$} (like in \cite{hoogeboom_tenPas97}) and write $\sh(\tau)$. As above, $\sh$ defines a mapping from $\texts\to\trees_{\Sigma\cup\Gamma}$.
\end{defn}

From the definitions of $\ttxt$ and $\sh$ immediately follows:
\begin{lem}\label{lem:txt_cat_sh}
	For every $\tau\in\texts$ it holds:
	\begin{equation*}
		(\ttxt\cat\sh)(\tau)=\tau.
	\end{equation*}
\end{lem}

We now have done all preparations to prove our Nivat characterization.

\begin{thm}[Nivat-like theorem for WBPAs]
	Let $K$ be a semiring. A text series $s:\texts\to K$ is recognized by a WBPA if and only if there exists a ranked alphabet $\Lambda$, a linear tree homomorphism $h:\trees_\Lambda\to\trees_{\Sigma\cup\Gamma}$, a regular tree language $L\subseteq\trees_\Lambda$, and a weighted tree automaton $\A_w$ over $\Lambda$ with exactly one state such that:
	\begin{equation*}
		%s(\tau)=h(\behav{\A_w}\hadamard\charac_L)(\sh(\tau))
		s=h(\behav{\A_w}\hadamard\charac_L)\cat\sh.
	\end{equation*}
\end{thm}
\begin{proof}
	\begin{description}
		\item[$\implies$] Let $\A$ be a WBPA such that $\behav{\A}=s$. By Mathissen \cite[Proposition 4.28]{mathissen09} we obtain a tree automaton $A_T$ with $\behav{\A_T}=\behav{\A}\cat\ttxt$. Applying \cref{thm:nivat_for_trees} yields the existence of a ranked alphabet $\Lambda$, a linear tree homomorphism $h:\trees_\Lambda\to\trees_{\Sigma\cup\Gamma}$, a regular tree language $L\subseteq\trees_\Lambda$, and a weighted tree automaton $\A_w$ with exactly one state such that:
		\begin{equation*}
			\behav{\A_T}=h(\behav{\A_w}\hadamard\charac_L).
		\end{equation*}
		Therefore we have
		\begin{alignat*}{2}
			&\behav{\A}\cat\ttxt&&=h(\behav{\A_w}\hadamard\charac_L) \\
			\implies&\behav{\A}\cat\ttxt\cat\sh&&=h(\behav{\A_w}\hadamard\charac_L)\cat\sh \vphantom{{\overset{\substack{\namecref{lem:txt_cat_sh}\\\labelcref{lem:txt_cat_sh}\mathstrut}}{\iff}}}\\
			\overset{\substack{\namecref{lem:txt_cat_sh}\\\labelcref{lem:txt_cat_sh}\mathstrut}}{\iff}&\behav{\A}&&=h(\behav{\A_w}\hadamard\charac_L)\cat\sh
		\end{alignat*}
		and thus
		\begin{equation*}
			s=\behav{\A}=h(\behav{\A_w}\hadamard\charac_L)\cat\sh.
		\end{equation*}
		\item[$\impliedby$] By \cref{thm:nivat_for_trees}, there exists a weighted tree automaton $\A_T$ such that $\behav{\A_T}=h(\behav{\A_w}\hadamard\charac_L)$. In \cite[Proposition 4.29]{mathissen09} Mathissen provides a construction for a WBPA $\A$ with $\behav{\A}=\behav{\A_T}\cat\sh$. Using this we get
		\begin{equation*}
			s=h(\behav{\A_w}\hadamard\charac_L)\cat\sh=\behav{\A_T}\cat\sh=\behav{\A},
		\end{equation*}
		which shows that $s$ is recognized by $\A$ and therefore regular, which finishes our proof.
	\end{description}
\end{proof}

Note that the relation between texts and trees from \cite[Propositions 4.28, 4.29]{mathissen09} used in our proof, could be used to further investigate weighted branching and parenthesizing automata and regular text series from a different viewpoint.