% !TeX spellcheck = en_US
\chapter{Preliminaries}
\label{ch:preliminaries}
To get a motivation for the following definitions we start with an example regarding natural language processing, inspired by \cite[Example 4.18]{ehrenfeucht_rozenberg93}.
\begin{ex}
	\label{ex:nlp}
	In an attempt to process natural languages, one approach is to analyze the syntactic structure of a given input phrase \cite{chomsky57}.
	A sentence, viewed as a linguistic unit of words, can be represented by a tree where each node is labeled by a category of the grammar, for example, a noun phrase (a phrase containing a noun). Consider the sentence ``Order does not come by itself''. The so-called ``constituency-based parse tree'' for this sentence could be drawn as follows:
	\begin{center}
		\begin{tikzpicture}[]
			\tikzset{frontier/.style={distance from root=150pt}}
			\Tree [.S [.\node(N-order){N}; Order ]
			[.\node(VP-1){VP}; [.\node(V-1){V}; [.\node(V-does){V}; does ]
			[.\node(ADV-not){ADV}; not ] ]
			[.\node(VP-2){VP}; [.\node(V-come){V}; come ]
			[.\node(PP){PP}; [.\node(P-by){P}; by ]
			[.\node(N-itself){N}; itself ] ] ] ] ]
		\end{tikzpicture}
	\end{center}
	The abbreviations stand for \textbf{S}entence, \textbf{N}oun (\textbf{P}hrase), \textbf{V}erb (\textbf{P}hrase) and \textbf{P}reposition (\textbf{P}hrase).
	But what if we now want to also represent the grammatical relations between the parts of the sentence? In our sentence there is an adverbial modification: ``does $\leftarrow$ not'' and a prepositional modification: ``by $\to$ itself''. There are also relations between phrases and words, such as the subject-predicate relation: ``Order $\to$ (does not come by itself)'', the relation between an auxiliary verb and a verb: ``(does not) $\to$ come'' and the relation between verb and object: ``(does not come) $\leftarrow$ by itself''. If we depict this relation $\rho$ by an arrow, we could modify our tree like this:
	\begin{center}
		\begin{tikzpicture}[sibling distance=25pt]
			\tikzset{frontier/.style={distance from root=150pt}}
			\Tree [.S [.\node(N-order){N}; Order ]
			[.\node(VP-1){VP}; 
			[.\node(VP-2){VP}; 
			[.\node(VP-3){VP}; 
			[.\node(V-does) {V}; does ] 
			[.\node(ADV-not) {ADV}; not ] ]
			[.\node(V-come) {V}; come ] ]
			[.\node(PP){PP}; [.\node(P-by){P}; by ]
			[.\node(N-itself){N}; itself ] ] ] ] 
			\draw[->] (N-order) edge[above] node {$\rho$} (VP-1)
			(PP) edge[above] node {$\rho$} (VP-2)
			(ADV-not) edge[above] node {$\rho$} (V-does)
			(VP-3) edge[above] node {$\rho$} (V-come)
			(P-by) edge[above] node {$\rho$} (N-itself);
		\end{tikzpicture}
	\end{center}
	We note that any sentence has a natural order of reading.
	The relation $\rho$ induces a second order on the words of the sentence if we say that the order of two words is inherited from the parent nodes. For our sentence ``Order does not come by itself'' we get the second order ``Order by itself not does come''. To represent the sentence and both of its orders by a data structure we introduce the formal notion of \textit{texts}. This enables the theoretical investigation of sentences with their grammatical relations as above.
\end{ex}
Texts were first introduced by Ehrenfeucht and Rozenberg as a special case of 2-structures \cite{ehrenfeucht_rozenberg93} and later treated by Hoogeboom and ten Pas \cite{hoogeboom_tenPas96}. Although their results were quite extensive, an automaton model to formally describe languages of texts was still missing. Only by the work of Mathissen \cite{mathissen07}, this piece was added. This thesis is inspired by these works and therefore uses many of the definitions and notions from them.
Texts are closely related to trees as they share a hierarchical structure. With nested words, we have already well-researched objects, which possess a similar second relation like texts. These similarities were of much use in the analysis of texts in \cite{mathissen09}.

In the first section we give a formal introduction to texts, their underlying biorders, and an important subset of them, the primitive biorders.
\cref{sec:texts_bounded_primitivity} will deal with an inductive structure on the set of texts, which will provide the basis of the following chapters. In assistance of understanding, we provide our own examples.

Let $\N=\set{0,1,2,\dots}$ and $\N*=\set{1,2,\dots}=\N\setminus\set{0}$. For $n\in\N$ we let $[n]:=\set{1,\dots,n}$. The set $\R$ consists of the real numbers and $\R_+$ of the non-negative real numbers. For a set $M$ we write $M^{\ast}$ for the set of strings over $M$.

\section{Biorders and Texts}
\label{sec:biorders_texts}
One can think of a word as a mapping from a finite linearly ordered set of positions $P\subset\N$ to an alphabet $\alphabet$. %We usually identify $P$ with $[n]$, equipped with the natural linear order on $\N$, for $n=\card{P}$.\\
Now, if we add a second linear order on $P$, we would be able to describe another structure on $P$ and therefore generalize words in a two-dimensional sense. With the following definitions, we will develop a formal basis for this concept and provide some basic properties.
\begin{defn}
	A \textit{biorder} is a tuple $\biorder$ of two linear orders $\leq_1,\leq_2$ over a common finite set $P$.
	The \textit{length} of a biorder $\pi$ is defined by $\abs{\pi}:=\card{P}$.
	We denote the only biorder of length 0 by $\varepsilon:=(\emptyset,\emptyset,\emptyset)$; here $\emptyset$ denotes the empty set and the empty relation, respectively.
\end{defn}
A linear order $\mathrm{\preceq}$ is typically given by its successor structure $\mathrm{\preceq}=(i_1,\dots,i_n)$ which is the ascending enumeration of all elements of $P$ according to $\mathrm{\preceq}$.
\begin{ex}\label{ex:biorders}
	The tuple $(\{r,s,t,u,v,w\},(s,v,r,t,u,w),(s,r,u,v,t,w))$ is a biorder of length 6 over the domain $P=\{r,s,t,u,v,w\}$. For simplicity we could replace the symbols from $P$ with natural numbers, preserving the linear orders. This results for example in the biorder $([6],(2,5,1,3,4,6),(2,1,4,5,3,6))$. Additionally, we could choose this relabeling so that the first order coincides with the natural order on $[6]$. We get $([6],(1,2,3,4,5,6),(1,3,5,2,4,6))$. We formalize this representation in the next paragraphs.
\end{ex}
\begin{defn}
	Two biorders $\pi_1=(P_1,\leq_1^{\pi_1},\leq_2^{\pi_1})$ and $\pi_2=(P_2,\leq_1^{\pi_2},\leq_2^{\pi_2})$ are \textit{isomorphic} if there exists a bijection $\varphi:P_1\to P_2$ such that $\varphi$ is an order isomorphism between $(P_1,\leq_1^{\pi_1})$ and $(P_2,\leq_1^{\pi_2})$, and $\varphi$ is an order isomorphism between $(P_1,\leq_2^{\pi_1})$ and $(P_2,\leq_2^{\pi_2})$. We call $\varphi$ a \textit{(biorder) isomorphism} from $\pi_1$ to $\pi_2$.
\end{defn}
In the following, we do not distinguish between isomorphic biorders.
It is well known that every finite ordered set permits an order isomorphism onto a finite subset $[n]$ of $\N$ equipped with the natural order $\leq$ of $[n]$.
This allows us to specify the first order of a biorder as the natural order of $[n]\subseteq\N$ and choose the second order accordingly. 
We achieve the following result:
\begin{prop}\label{prop:standard_biorders}
	Each biorder $\pi=\biorder$ is isomorphic to a standard biorder $([n],(1,\dots,n),(i_1,\dots,i_n))$ for $n=\abs{\pi}$. 
\end{prop}
\begin{proof}
	Let $\varphi:P\to[n]$ be the order isomorphism between $(P,\leq_1)$ and $([n],\leq)$, which exists because $P$ is finite. Then $\varphi$ is also an order isomorphism between $(P,\leq_2)$ and $([n],\leq')$, where $\leq'$ is given by
	\begin{equation*}
		\leq':=\set[(k,l)]{\varphi^{-1}(k)\leq_2\varphi^{-1}(l)}
	\end{equation*}
	or its successor structure $(\varphi^{-1}(1),\dots,\varphi^{-1}(n))$, because for $x,y\in P$
	\begin{equation*}
		x\leq_2 y\implies\varphi^{-1}(\varphi(x))\leq_2\varphi^{-1}(\varphi(y))\implies \varphi(x)\leq'\varphi(y)
	\end{equation*}
	holds. Setting $i_j:=\varphi^{-1}(j)$ provides the desired result.
\end{proof}
From now on we use biorders in standard form and omit the domain by only stating the tuple $(i_1,\dots,i_n)$. One should note that this is possible because we are not dealing with partial orders. This standard form gives us a simple way to represent biorders by a graph: We draw nodes for each domain element and connect them by arrows, ascending by the second order. The first order is depicted by the numbering of the nodes and also by drawing them from left to right ascending by the first order. This is important for the representation of texts, with which we will deal after the next example. 
\begin{ex}\label{ex:standard_biorders}
	%Let $\alphabet$ be an alphabet.
	The biorder from \cref{ex:biorders} could be written as $(1,3,5,2,4,6)$ and drawn like this:
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\def \k {6}
			\foreach \x in {1,...,\k}
			\coordinate (\x) at (\x,0);
			\node[draw] (1) at (1,0) {1};
			\node[draw] (2) at (2,0) {2};
			\node[draw] (3) at (3,0) {3};
			\node[draw] (4) at (4,0) {4};
			\node[draw] (5) at (5,0) {5};
			\node[draw] (6) at (6,0) {6};
			\draw (1) to[arcleft] (3);
			\draw (3) to[arcleft] (5);
			\draw (5.north) to[bend right=65] (2.north);
			\draw (2) to[arcright] (4);
			\draw (4) to[arcright] (6);
		\end{tikzpicture}.
	\end{center}
	The only biorder of length 1 is $(1)$, which we denote by 1 if it is clear that we refer to the biorder and not the number. The biorders $\hor:=(1,2)$ and $\ver=:(2,1)$ are the only ones of length 2. We visualize them by:
	\begin{center}
		\hfill
		$\hor:$ \begin{tikzpicture}[biorder,baseline=(1.base)]
		\node[draw] (1) at (1,0) {1};
		\node[draw] (2) at (3,0) {2};
		\draw (1) -- (2);
		\end{tikzpicture}
		\hfill
		$\ver:$ \begin{tikzpicture}[biorder,baseline=(1.base)]
		\node[draw] (1) at (1,0) {1};
		\node[draw] (2) at (3,0) {2};
		\draw (2) -- (1);
		\end{tikzpicture}.
		\hspace*{\fill}
	\end{center}
	We call $\hor$ the \textit{horizontal} or \textit{forward sequential} biorder, and $\ver$ the \textit{vertical} or \textit{backward sequential} biorder.
	One could easily check that there are 6 biorders of length 3. In general each biorder of length $n$ is directly connected to a permutation of $[n]$ and therefore it does not surprise that there are $n!$ of such biorders.
\end{ex}
\begin{defn}
	A \textit{text} over an alphabet $\alphabet$ (a finite set of symbols) is a biorder with a labeling from $\alphabet$, i.e. a tuple $\txt$ of two linear orders $\leq_1,\leq_2$ over a common finite domain $P$ and a function $\lambda:P\to\alphabet$ called the \textit{labeling function}. We do not specify the alphabet if it is not significant.
	The \textit{length} of a text $\tau$ is defined by $\abs{\tau}:=\card{P}$.
	We denote the only text of length 0 by $\varepsilon:=(\emptyset,\emptyset,\emptyset,\emptyset)$, here $\emptyset$ denotes the empty set, the empty function, and the empty relation, respectively. The distinction between $\varepsilon$ as a biorder or as a text will be clear from the context. We use the abbreviation $a$ for the singleton text $(a,(1))$ if it is clear that we refer to the text and not the symbol $a\in\alphabet$.
	Two texts $\tau_1=(P_1,\lambda_1,\leq_1^{\tau_1},\leq_2^{\tau_1})$ and $\tau_2=(P_2,\lambda_2,\leq_1^{\tau_2},\leq_2^{\tau_2})$ are \textit{isomorphic} via $\varphi:P_1\to P_2$ if their underlying biorders are isomorphic via $\varphi$ and additionally $\lambda_1(x)=\lambda_2(\varphi(x))$ for all $x\in P_1$ holds. As for biorders, we do not distinguish between isomorphic texts.
	The \textit{set of all texts} over $\alphabet$ is called $\texts*$, if we exclude the empty text $\varepsilon$ we write $\textsp*$. A subset $L\subseteq\texts*$ is called a \textit{(text) language}.
\end{defn}
Like in the case of biorders, we assume a standard form of texts $\tau=([n],\lambda,\allowbreak(1,\dots,n),\allowbreak(i_1,\dots,i_n))$. Its existence follows directly from the proof of \cref{prop:standard_biorders} because $\lambda(j)=\lambda(\varphi(i_j))$ (we kept the same labeling function). Therefore we represent a text by the pair $(\lambda(1),\dots,\lambda(n),(i_1,\dots,i_n))$, namely the enumeration of the values of $\lambda$ and the successor structure of the second order. Graphically texts are depicted like biorders, but instead of numbering the nodes, we write down their label. The first order is then given implicitly by the arrangement of the nodes.
\begin{ex}\label{ex:texts}
	From our previous biorder $(1,3,5,2,4,6)$ we construct a text over $\alphabet=\{a,b,c\}$ by adding a labeling function $\lambda:[6]\to\alphabet$ with $\lambda(1)=\lambda(2)=a,\lambda(3)=\lambda(4)=b$ and $\lambda(5)=\lambda(6)=c$. The standard form of this text is $(aabbcc,(1,3,5,2,4,6))$. A graphical representation could look like this:
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\def \k {6}
			\foreach \x in {1,...,\k}
			\coordinate (\x) at (\x,0);
			\node[draw] (1) at (1,0) {a};
			\node[draw] (2) at (2,0) {a};
			\node[draw] (3) at (3,0) {b};
			\node[draw] (4) at (4,0) {b};
			\node[draw] (5) at (5,0) {c};
			\node[draw] (6) at (6,0) {c};
			\draw (1) to[arcleft] (3);
			\draw (3) to[arcleft] (5);
			\draw (5.north) to[bend right=65] (2.north);
			\draw (2) to[arcright] (4);
			\draw (4) to[arcright] (6);
		\end{tikzpicture}.
	\end{center}
The labels arranged by the second order are $abcabc$. This example shows that a text is more than just a word and a corresponding anagram because from the pair $(aabbcc,abcabc)$ we could derive multiple possible texts, for example, $\tau_1=(aabbcc,\allowbreak(2,4,6,1,3,5))$ and $\tau_2=(aabbcc,(2,3,6,1,4,5))$, but $\tau_1\not\simeq\tau_2$ and $\tau_1\neq\tau_2$ especially. Any isomorphism which swaps 3 and 4 (to achieve the same second order), results in a non-isomorphic first order. This problem arises from the fact that we cannot distinguish between the same letters, but between their positions.
\end{ex}
Our goal for the rest of this chapter is to elaborate on an inductive structure for the set of texts. To achieve this we will investigate primitive elements from which all texts could be constructed by simple operations.
\begin{defn}
	For a biorder $\pi=\biorder$ and some $i,j\in P$ with $i\leq_1 j$ we denote the interval $\set[k\in P]{i\leq_1 k\leq_1 j}$ by $[i,j]_1$ and call it an \textit{interval of the first order}. The interval $[i,j]_2$, an \textit{interval of the second order}, is defined analogously.
	A subset of $P$ which is an interval of both orders is called a \textit{clan}. Clans that are strict subsets of $P$ are called \textit{proper}. We call $\pi$ \textit{primitive} if its domain contains at least two elements and has only trivial clans, i.e. the singletons and the domain itself.
	We apply the same notions for texts and refer to the underlying biorder.
\end{defn}
\begin{ex}\label{ex:piorders}
	Our biorder $(1,3,5,2,4,6)$ and hence every associated text are not primitive, because they contain the clan $[2,5]_1=[3,4]_2$, which is easily seen from the graph:
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\def \k {6}
			\foreach \x in {1,...,\k}
			\coordinate (\x) at (\x,0);
			\node[draw] (1) at (1,0) {1};
			\node[draw,clan] (2) at (2,0) {2};
			\node[draw,clan] (3) at (3,0) {3};
			\node[draw,clan] (4) at (4,0) {4};
			\node[draw,clan] (5) at (5,0) {5};
			\node[draw] (6) at (6,0) {6};
			\draw (1) to[arcleft] (3);
			\draw[clan] (3) to[arcleft] (5);
			\draw[clan]  (5.north) to[bend right=65] (2.north);
			\draw[clan]  (2) to[arcright] (4);
			\draw (4) to[arcright] (6);
		\end{tikzpicture}.
	\end{center}
	Clearly, the two biorders $\hor$ and $\ver$ from \cref{ex:standard_biorders} are both primitive. All of the 6 biorders of length 3 contain at least one non-trivial clan, for example, $(3,2,1)$ contains the clan $\set{1,2}$, hence none of them is primitive. One can easily determine the 2 primitive biorders $(2,4,1,3)$ and $(3,1,4,2)$ of cardinality 4 by hand, but then at the latest the question arises whether there is an expression for the number of primitive biorders of length $n$.
\end{ex}
At first, we answer the simpler question of how many primitive biorders (of all lengths) exist.
\begin{prop}[\cite{mathissen09}]
	The cardinality of the set of all primitive biorders is $\aleph_0=\card{\N}$.
\end{prop}
\begin{proof}
	As each biorder of length $n$ is represented by a permutation of $[n]$, we see that there are at most $\aleph_0$ (primitive) biorders.
	
	For $n\geq2$ we set $\pi_n:=(2,4,\dots,2n,1,3,\dots,2n-1)$ a biorder of length $2n$.
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\def \k {6}
			\foreach \x in {1,...,\k}
			\node[draw] (\x) at (\x,0) {\x};
			\node (7) at (\k+1,0) {\phantom{7}};
			\node (8) at (\k+2,0) {\phantom{8}};
			\node (...) at (\k+2.5,0) {\dots};
			\node (2n-4) at (\k+3,0) {\phantom{2n-4}};
			\node (2n-3) at (\k+4,0) {\phantom{2n-3}};		
			\node[draw] (2n-2) at (\k+5.5+0.5,0) {2n-2};
			\node[draw] (2n-1) at (\k+7.5+0.5,0) {2n-1};
			\node[draw] (2n) at (\k+9+0.5,0) {2n};
			\draw (1) to[bend left=65,looseness=0.5] (2n);
			\draw (2n) to[arcright] (2n-2);
			\draw (2n-2) to[arcright] (2n-4);
			\draw (8) to[arcright] (6);
			\draw (6) to[arcright] (4);
			\draw (4) to[arcright] (2);
			\draw (2n-1) to[arcleft] (2n-3);
			\draw (7) to[arcleft] (5);
			\draw (5) to[arcleft] (3);
			\draw (3) to[arcleft] (1);
		\end{tikzpicture}
	\end{center}
	Let  $I\subseteq[2n]$ be an interval of $\pi_n$ of both orders that is not a singleton. Therefore $I$ has to contain $i,i+1$ for some $i\in\set{1,\dots,2n-1}$. Since either $i\leq_2 2n\leq_2 1\leq_2 i+1$ or $i+1\leq_2 2n\leq_2 1\leq_2 i$ also $1$ and $2n$ are in $I$. This means that $I=[2n]$ is the only clan of $\pi_n$ apart from the singletons. Thus $\pi_n$ does not contain non-trivial clans and is hence primitive. We have found $\aleph_0$ different primitive biorders and this concludes the proof.
\end{proof}
In an attempt to answer the initial question, we use the already mentioned connection between biorders and permutations in group theory. This link is directly seen when we interpret our short notation of biorders as the classical one-line notation of permutations of $[n]$, not to be confused with the cycle notation. Our definition of primitive biorders can then be directly transferred and matches with the notion of \textit{simple permutations}, which were investigated by Albert, Atkinson, and Klazar \cite{albert_atkinson_klazar_simple_permutations} as the building blocks of all permutations. They proved the following result, originally for simple permutations:
\begin{prop}[Albert, Atkinson, Klazar, 2003]
	The number of primitive biorders of length $n$ for $n\geq4$ is exactly
	\begin{equation*}
		-\operatorname{Com}_n+2\cdot(-1)^{n+1},
	\end{equation*}
	where $\operatorname{Com}_n$ is the coefficient of $t^n$ in the inverse power series $F^{-1}$ of $F(t)=\sum_{n\geq1}n!\cdot t^n$, examined by Comtet \cite{comtet72}.
\end{prop}
Together with the cases for $n<4$, we can summarize: There are no primitive biorders of length 0 and 1, two primitive biorders of length 2, no primitive biorder of length 3, two of length 4, and after that, the sequence continues with 6, 46 and 338. For an 
extensive list and more insight refer to \cite{oeis_simple_perms05}.

\section{Texts of Bounded Primitivity}
\label{sec:texts_bounded_primitivity}
With our investigations of primitive biorders, we are now able to provide an inductive structure for texts, which is built by repeatedly substituting texts $\tau_1,\dots,\tau_n$ into a primitive biorder $\pi$ where each $\tau_i$ replaces one domain element of $\pi$.
\begin{defn}\label{def:bounded_primitivity}
	For a biorder $\pi$ of length $n$ and texts $\tau_1,\dots,\tau_n$ over an alphabet $\alphabet$ we define the text $\tau=\pi(\tau_1,\dots,\tau_n)$ over the same alphabet as the \textit{substitution} of $\tau_1,\dots,\tau_n$ in $\pi$, i.e. we assume, without loss of generality, mutually disjoint domains $P_i$ for $\tau_i=(P_i,\lambda^{\tau_i},\leq_1^{\tau_i},\leq_2^{\tau_i}),i\in[n]$ and set $\tau=(\bigcup_{i\in[n]}P_i,\lambda,\leq_1,\leq_2)$ where we define for $k\in P_i,k'\in P_{j}$:
	\begin{align*}
		\lambda(k)&:=\lambda^{\tau_i}(k)\\
		k\leq_1 k'\text{ if }&\begin{array}[t]{l}
			i<_1^{\pi}j\text{ or }\\
			i=j\text{ and }k\leq_1^{\tau_i}k'
		\end{array}\\
		k\leq_2 k'\text{ if }&\begin{array}[t]{l}
			i<_2^{\pi}j\text{ or }\\
			i=j\text{ and }k\leq_2^{\tau_i}k'.
		\end{array}
	\end{align*}
	Let $\piorders*$ be a set of primitive biorders and $\alphabet$ an alphabet. The set $\texts$ of texts over $\alphabet$ \textit{generated by} $\piorders*$ is the smallest subset of $\texts*$ such that:
	\begin{itemize}
		\item $\varepsilon\in\texts$,
		\item $(a,(1))\in\texts$ for $a\in\alphabet$ and 
		\item $\pi(\tau_1,\dots,\tau_n)\in\texts$ for $\pi\in\piorders*$ with length $n$ and $\tau_1,\dots,\tau_n\in\texts\setminus\{\varepsilon\}$.
	\end{itemize}
	Analogously to $\textsp*$ we define $\textsp:=\texts\setminus\{\varepsilon\}$. 
	We call the representation of a text $\tau$ in terms of the singleton texts and primitive biorders from $\piorders*$ a \textit{primitive representation} of $\tau$.
	If $\piorders*$ is finite, we call a $L\subseteq\texts$ a (text) language of \textit{bounded primitivity}.
	From now on we mostly use biorders as blueprints for substituting texts into, and therefore refer to the length of a biorder $\pi$ by $\rk(\pi)$, the \textit{rank} of $\pi$.
\end{defn}
We adopt the definition of substitution for biorders (by ignoring the labeling) and allow the substitution of biorders into biorders.

Let us consider an important set of texts. Ehrenfeucht, ten Pas and Rozenberg \cite{ehrenfeucht_tenPas_rozenberg93} called the set $\operatorname{ALT}(\Sigma):=\texts[\alphabet][\{\hor,\ver\}]$ the \textit{alternating texts}. Ésik and Németh \cite{esik_nemeth02} examined the set of \textit{$\alphabet$-labeled series-parallel-biposets} $\operatorname{SPB}(\alphabet)$ which happens to be isomorphic to $\operatorname{ALT}(\Sigma)$. These sets are strict subsets of $\texts[\alphabet][\piorders*]$ if $\piorders*$ is a strict superset of $\{\hor,\ver\}$, see the second part of the following example for details.
\begin{ex}\label{ex:biorder_substitution}
	The three texts $\tau_1=(ab,(1,2)),\tau_2=(bc,(2,1))$ and $\tau_3=(cd,(2,1))$ over $\{a,b,c,d\}$, visualized by
	\begin{center}
		\hfill
		$\tau_1=$ \begin{tikzpicture}[biorder,baseline=(1.base)]
			\node[draw] (1) at (1,0) {a};
			\node[draw] (2) at (3,0) {b};
			\draw (1) to (2);
		\end{tikzpicture} ,
		\hfill
		$\tau_2=$ \begin{tikzpicture}[biorder,baseline=(1.base)]
			\node[draw] (1) at (1,0) {b};
			\node[draw] (2) at (3,0) {c};
			\draw (2) to (1);
		\end{tikzpicture} ,
		\hfill
		$\tau_3=$ \begin{tikzpicture}[biorder,baseline=(1.base)]
			\node[draw] (1) at (1,0) {c};
			\node[draw] (2) at (3,0) {d};
			\draw (2) to (1);
		\end{tikzpicture} ,
		\hspace*{\fill}
	\end{center}
	could be substituted into the primitive biorders $\hor$ and $\ver$ to generate new texts.
	For example $\hor(\tau_1,\tau_2)=(abbc,(1,2,4,3))$ and $\ver(\hor(\tau_1,\tau_2),\tau_3)=(abbccd,\allowbreak(6,5,1,2,4,3))$, visualized by 
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\node[draw] (1) at (1,0) {a};
			\node[draw] (2) at (2,0) {b};
			\node[draw] (3) at (3,0) {b};
			\node[draw] (4) at (4,0) {c};
			\node[draw] (5) at (5,0) {c};
			\node[draw] (6) at (6,0) {d};
			\draw (6) to[arcright] (5);
			\draw (5) to[arcright] (1);
			\draw (1) to[arcright] (2);
			\draw (2) to[arcright] (4);
			\draw (4) to[arcright] (3);
		\end{tikzpicture}.
	\end{center}
\end{ex}
From now on, we use the infix notation for these biorders of length 2 and thus write $\tau_1\hor\tau_2$ for $\hor(\tau_1,\tau_2)$ and $\tau_1\ver\tau_2$ for $\ver(\tau_1,\tau_2)$. We may not change the order of calculation in $(\tau_1\hor\tau_2)\ver\tau_3$ to $\tau_1\hor(\tau_2\ver\tau_3)$ and expect the same outcome. In fact, $\hor$ and $\ver$ are both associative operations \cite[p. 58]{mathissen09} but do not share a joint associativity law. Hence we can only omit the parentheses in expressions containing either only $\hor$ or $\ver$.
\begin{ex}
	As we know from \cref{ex:piorders} the text $(aabbcc,(1,3,5,2,4,6))$ is not primitive, but we can find a primitive representation by replacing the clan $[2,5]_1$ with the primitive biorder $\pi=(2,4,1,3)$
	\begin{center}
		\begin{tikzpicture}[biorder,baseline=(1.base)]
			\def \k {6}
			\foreach \x in {1,...,\k}
			\coordinate (\x) at (\x,0);
			\node[draw] (1) at (1,0) {a};
			\node[draw,clan] (2) at (2,0) {a};
			\node[draw,clan] (3) at (3,0) {b};
			\node[draw,clan] (4) at (4,0) {b};
			\node[draw,clan] (5) at (5,0) {c};
			\node[draw] (6) at (6,0) {c};
			\draw (1) to[arcleft] (3);
			\draw[clan] (3) to[arcleft] (5);
			\draw[clan] (5.north) to[bend right=65] (2.north);
			\draw[clan] (2) to[arcright] (4);
			\draw (4) to[arcright] (6);
			\draw[decorate,decoration={calligraphic brace,amplitude=10pt,mirror,raise=20pt},-,line width=1.25pt] (2.west) -- (5.east) node[midway,yshift=-45pt] {\pi(a,b,b,c)};
		\end{tikzpicture}
	\end{center}
	and get the representation $(a\hor\pi(a,b,b,c))\hor c=a\hor\pi(a,b,b,c)\hor c$.
	
	Let $\pi$ be a primitive biorder of length $\abs{\pi}\geq4$. The text $\tau=\pi(a,\dots,a)$ is not in $\texts[\{a\}][\{\hor,\ver\}]$, but obviously in $\texts[\{a\}][\{\hor,\ver,\pi\}]$. To see the former, notice that the underlying biorder $\pi$ of $\tau$ is primitive and distinct from $\hor$ and $\ver$, and hence there is no representation of $\pi$ in terms of $\hor$ and $\ver$.	
\end{ex}
\begin{lem}\label{lem:text_decomposition}
	For every text $\tau$ with $\abs{\tau}\geq2$ there exists a unique primitive biorder $\pi$, and texts $\tau_1,\dots,\tau_k\in\textsp*$ such that
	\begin{equation*}
		\tau=\pi(\tau_1,\dots,\tau_k).
	\end{equation*}
	Moreover, if $\ver\neq\pi\neq\hor$ then $\tau_1,\dots,\tau_k$ are also uniquely determined. If $\pi=\hor$ or $\pi=\ver$ then $\tau_1$ and $\tau_2$ are uniquely determined up to associativity.
\end{lem}
\begin{proof}
	Suppose that $\tau=([n],\lambda,\leq_1,\leq_2)$ has two distinct maximal proper clans $A$ and $B$ that have a non-empty intersection. As the union of intersecting intervals, $A\cup B$ is an interval (of both orders) and therefore a clan. Because of the maximality, $A\cup B=[n]$. But it is also clear that $A$ and $B$ cannot be subsets of each other, otherwise, we would have $A=[n]$ or $B=[n]$ which is a contradiction to $A$ and $B$ being proper. In other words we have $\tau=\pi(\tau_1,\tau_2)$ with $\pi=(1,2)$ or $\pi=(2,1)$, where the domain of $\tau_1$ contains $A\setminus B$ and the domain of $\tau_2$ contains $B\setminus A$. The allocation of $A\cap B$ to $\tau_1$ or $\tau_2$ is arbitrary, but every such choice of $\tau_1$ and $\tau_2$ results in the same primitive representation of $\tau$ if we identify associative expressions.
	
	We suppose now that every pair of distinct maximal proper clans has an empty intersection. Then these maximal clans $A_1,\dots,A_k$ for $k\geq2$ form a disjoint decomposition of $P=\bigcup_{1\leq i\leq k}A_i$. We set $\tau_i:=(A_i,\restr{\lambda}{A_i},\restr{\leq_1}{A_i},\restr{\leq_2}{A_i})$ for $1\leq i\leq k$, which are well defined because the $A_i$ are intervals of both orders. Since clans are never empty, $\tau_i\neq\varepsilon$ holds. We assume the $A_i$ to be ordered according to their position if $[n]$ is ordered by $\leq_1$. Thus, the decomposition is uniquely determined. Any attempt to get a coarser decomposition (i.e. with fewer clans) must fail, because of the maximality of the clans. If we now look at the relative order of the clans, we get a biorder $\pi=(\set[A_i]{1\leq i\leq k},\leq_1^{\pi},\leq_2^{\pi})$ where $\leq_1^{\pi}$ has the successor structure $(A_1,\dots,A_k)$ and $\leq_2^{\pi}$ arises according to the position of the clans if $[n]$ is ordered by $\leq_2$. The existence of a non-trivial clan in $\pi$ would imply the existence of a clan in $\tau$ which is the union of at least two $A_i$, in contradiction to the maximality requirement. Therefore $\pi$ must be a primitive biorder and the texts $\tau_i$ are the restrictions of $\tau$ to the domains $A_i$. Clearly the $\tau_i$ are uniquely determined.
\end{proof}
\begin{ex}
	The text $\tau=(aabbcc,(1,3,5,2,4,6))$ from \cref{ex:texts} contains the clans $[1,6]_1,[1,4]_1,[2,5]_1,[2,6]_1$ and the singleton clans $[i,i]_1,i\in[6]$. Apart from the trivial ones, the clans $[1,4]_1,[2,5]_1$ and $[2,6]_1$ remain. With $\tau'= (abbcc, (2,4,1,3,5))$ we get $\tau=\hor(a,\tau')$ from the proof above. Applying the procedure again to $\tau'$ results in $\tau=\hor(a,\hor(\tau'',c))$ with $\tau''=(abbc, (2,4,1,3))$ which is constructed from the primitive biorder $\pi=(2,4,1,3)$. In conclusion we get $\tau=\hor(a,\hor(\pi(a,b,b,c),c))=a\hor\pi(a,b,b,c)\hor c$ as we already found out in \cref{ex:biorder_substitution}.
\end{ex}
From \Cref{lem:text_decomposition} we inductively get that all $\tau\in\texts$ have a primitive representation if $\piorders*$ contains all primitive biorders. Thus, we get the following result, which was also proven by Ehrenfeucht, ten Pas and Rozenberg \cite{ehrenfeucht_tenPas_rozenberg93} by using the theory of 2-structures from \cite{ehrenfeucht_rozenberg90}. 
\begin{thm}
	Let $\alphabet$ be an alphabet. If $\piorders*$ contains all primitive biorders, then
	\begin{equation*}
		\texts=\texts*.
	\end{equation*}
\end{thm}
We conclude this chapter with two examples. The first shows that texts are indeed a generalization of words, like we originally introduced them.
\begin{ex}\label{ex:words_as_texts}
	Let $\alphabet$ be an alphabet. We can identify each word $w\in\alphabet*$ with a text
	\begin{align*}
		\tau=(w,(1,\dots,\abs{w}))
	\end{align*}
	from $\texts[\alphabet][\set{\hor}]$ and vice versa, thus $\alphabet*\cong\texts[\alphabet][\set{\hor}]$. This shows that by choosing $\piorders*=\set{\hor}$, the texts over $\alphabet$ generated by $\piorders*$ represent exactly the words over $\alphabet$.
\end{ex}
Let us now look back at \cref{ex:nlp} from the beginning of this chapter. 
\begin{ex}
	With our framework we are now able to represent the grammatical structure of the sentence ``Order does not come by itself'' as a text:
	\begin{equation*}
		\text{Order}\hor(\text{does}\ver\text{not})\hor(\text{come}\ver(\text{by}\hor\text{itself}))
	\end{equation*}
	over the alphabet $\alphabet=\{\text{by, come, does, itself, not, Order}\}$.
\end{ex}